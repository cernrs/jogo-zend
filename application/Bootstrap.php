<?php


class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

  /**
   * Configura os módulos
   */
  protected function _initAutoload() {

    $oIniConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
    $aIniConfig = $oIniConfig->toArray();
    $aModules   = array();

    if (isset($aIniConfig['production']['app']['module'])) {
      $aModules = $aIniConfig['production']['app']['module'];
    }

    // Estancia os modulos
    foreach ($aModules as $sModule => $sNamespace) {

      $oModule = new Zend_Application_Module_Autoloader(array(
                                                          'namespace' => (String) ucfirst($sNamespace),
                                                          'basePath'  => APPLICATION_PATH . "/modules/{$sModule}"
                                                        ));
      $oModule->addResourceTypes(array(
                                   'library'   => array('path' => 'library/', 'namespace' => 'Lib'),
                                   'dao'       => array('path' => 'dao/', 'namespace' => 'Dao'),
                                   'form'      => array('path' => 'forms/', 'namespace' => 'Form'),
                                   'model'     => array('path' => 'models/', 'namespace' => 'Model'),
                                   'interface' => array('path' => 'interfaces/', 'namespace' => 'Interface')
                                 ));
    }
  }

    // public function _initAutoloader()
    // {
    //     $loader = Zend\Loader\Autoloader::getInstance();
    //     $loader->setFallbackAutoloader(true);
    // }

    // protected function _initDatabase()
    // {
    //     $adapter = Zend_Db::factory('pdo\_pgsql', array(
    //         'host' => 'localhost',
    //         'username' => 'postgres',
    //         'password' => '',
    //         'dbname' => 'teste',
    //         'port' => '5432',
    //         'charset' => 'utf8'
    //     ));
    //     Zend_Db_Table_Abstract::setDefaultAdapter($adapter); // setting up the db adapter to DbTable
    // }


}

?>
